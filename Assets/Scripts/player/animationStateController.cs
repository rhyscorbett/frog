using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationStateController : MonoBehaviour
{




    float velocity = 0.0f;
    Animator animator;
    public float speed;
    int VelocityHash;

    int speedHash;

    Vector3 oldPosition;



    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        VelocityHash = Animator.StringToHash("Velocity");
    }

    // Update is called once per frame
    void Update()
    {

        float speed = Vector3.Distance(oldPosition, transform.position) * 100f;
        oldPosition = transform.position;
        Debug.Log("speed: " + speed.ToString("F2"));



        bool forwardPressed = (speed <= 20f);
        bool runPressed = (speed > 20f);


        if (forwardPressed && velocity<120f)
        {
            velocity = speed;
        }

        if (!forwardPressed && velocity > 0.0f)
        {
            velocity = speed;
        }


        animator.SetFloat(VelocityHash, velocity);




    }

    void FixedUpdate()
    {

    }
}
