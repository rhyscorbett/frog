using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    private float healthAmount = 20;

    private void Update()
    {
        if (healthAmount <= 0)
        {
            GameObject.Find(transform.root.gameObject.name + ("spawn point")).GetComponent<EnemyRespawn>().Death = true;

            killSelf();
        }

        
    }

    public bool isTouchingPlayer;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8) //check the int value in layer manager
        {
            isTouchingPlayer = true;
            TakeDamage(20);
        }
    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            isTouchingPlayer = false;
        }
    }

    public void TakeDamage(float Damage)
    {
        healthAmount -= Damage;
        
    }

    public void Healing(float healPoints)
    {
        healthAmount += healPoints;
        healthAmount = Mathf.Clamp(healthAmount, 0, 100);

        Invoke("Respawn", 5);
    }

    private void killSelf()
    {
        Destroy(gameObject);
        Destroy(transform.root.gameObject);
    }

    void Respawn()
    {

    }

}