using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public Image healthBar;
    public float healthAmount = 100;

    private void Update()
    {
        if (healthAmount <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            TakeDamage(20);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            Healing(10);
        }
    }

    public bool isTouchingEnemy;
    public bool isTouchingHeart;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9) //check the int value in layer manager
        {
            isTouchingEnemy = true;
            TakeDamage(20);
        }

        else if (collision.gameObject.layer == 11) //check the int value in layer manager
        {
            isTouchingHeart = true;
            Healing(10);
        }

    }
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isTouchingEnemy = false;
        }

        else if (collision.gameObject.layer == 11)
        {
            isTouchingHeart = false;
        }

    }






    public void TakeDamage(float Damage)
    {
        healthAmount -= Damage;
        healthBar.fillAmount = healthAmount / 100;
    }

    public void Healing(float healPoints)
    {
        healthAmount += healPoints;
        healthAmount = Mathf.Clamp(healthAmount, 0, 100);

        healthBar.fillAmount = healthAmount / 100;
    }

}